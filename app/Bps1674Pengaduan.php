<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bps1674Pengaduan extends Model
{
    protected $table = 'bps1674_pengaduan';
    
    public function attributes()
    {
        return (new \App\Http\Requests\Bps1674PengaduanRequest())->attributes();
    }
    
    public function CreatedUser()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
