<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bps1674PerusahaanKunjungan extends Model
{
    protected $table = 'bps1674_perusahaan_kunjungan';
    
    public function attributes()
    {
        return (new \App\Http\Requests\Bps1674PerusahaanKunjunganRequest())->attributes();
    }
}
