<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Bps1674PengaduanRequest;
use App\Http\Requests\Bps1674PengaduanResponRequest;
use Illuminate\Support\Facades\DB;

class PengaduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $datas = \App\Bps1674Pengaduan::where('judul', 'LIKE', '%' . $keyword . '%')
            ->orWhere('isi', 'LIKE', '%' . $keyword . '%')
            ->paginate();

        $datas->withPath('pengaduan');
        $datas->appends($request->all());

        if ($request->ajax()) {
            return \Response::json(\View::make('pengaduan.list', array('datas' => $datas))->render());
        }

        return view('pengaduan.index',compact('datas', 'keyword'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model= new \App\Bps1674Pengaduan;
        return view('pengaduan.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Bps1674PengaduanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect('pengaduan/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $model= new \App\Bps1674Pengaduan;
        $model->judul=$request->get('judul');
        $model->isi=$request->get('isi');
        $model->created_by=Auth::id();
        $model->updated_by=Auth::id();
        $model->save();
        
        return redirect('pengaduan')->with('success', 'Information has been added');
    }

    public function create_respon($id)
    {
        $parent = \App\Bps1674Pengaduan::find($id);
        $model= new \App\Bps1674PengaduanRespon;
        return view('pengaduan.create_respon', compact('model', 'parent'));
    }

    public function store_respon($id,Bps1674PengaduanResponRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect('pengaduan/create_respon')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $model= new \App\Bps1674PengaduanRespon;
        $model->pengaduan_id=$id;
        $model->isi=$request->get('isi');
        $model->created_by=Auth::id();
        $model->updated_by=Auth::id();
        $model->save();
        
        return redirect('pengaduan/'.$id.'/detail')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $model = \App\Bps1674Pengaduan::find($id);

        $datas = \App\Bps1674PengaduanRespon::where('pengaduan_id', '=', $id)
            ->paginate();

        $datas->withPath('pengaduan');

        return view('pengaduan.detail',compact('model','id', 'datas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = \App\Bps1674Pengaduan::find($id);
        return view('pengaduan.edit',compact('model','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Bps1674PengaduanRequest $request, $id)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect('pengaduan/edit',$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $model= \App\Bps1674Pengaduan::find($id);
       
        $model->judul=$request->get('judul');
        $model->isi=$request->get('isi');
        $model->updated_by=Auth::id();
        $model->save();
        return redirect('pengaduan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Bps1674PengaduanRespon::where('pengaduan_id',$id)->delete();
        $model = \App\Bps1674Pengaduan::find($id);
        $model->delete();
        return redirect('pengaduan')->with('success','Information has been  deleted');
    }
}
