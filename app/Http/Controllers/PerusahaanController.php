<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Bps1674PerusahaanRequest;
use App\Http\Requests\Bps1674PerusahaanKunjunganRequest;
use App\Http\Requests\Bps1674PerusahaanSurveyRequest;
use Illuminate\Support\Facades\DB;

class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nama_perusahaan = $request->get('nama_perusahaan');
        $nama_survey = $request->get('nama_survey');
        $tahun_mulai = $request->get('tahun_mulai');
        $notasi_pendapatan = $request->get('notasi_pendapatan');
        $notasi_tahun = $request->get('notasi_tahun');
        $pendapatan = $request->get('pendapatan');
        $kategori = $request->get('kategori');

        $arr_where = [];

        if(strlen($nama_perusahaan)>0){
            $arr_where[] = ['bps1674_perusahaan.nama', 'LIKE', '%'.$nama_perusahaan.'%'];
        }
        
        if(strlen($nama_survey)>0){
            $arr_where[] = ['bps1674_perusahaan_survey.nama_survey', 'LIKE', '%'.$nama_survey.'%'];
        }
        
        if(strlen($tahun_mulai)>0){
            $list_notasi = ['>=', '=', '<='];
            $arr_where[] = ['bps1674_perusahaan.tahun_mulai', $list_notasi[$notasi_tahun], $tahun_mulai];
        }
        
        if(strlen($kategori)>0){
            $arr_where[] = ['bps1674_perusahaan.kategori', '=', $kategori];
        }
        
        if(strlen($pendapatan)>0){
            $list_notasi = ['>=', '=', '<='];
            $arr_where[] = ['bps1674_perusahaan_survey.pendapatan', $list_notasi[$notasi_pendapatan], $pendapatan];
        }

        $datas = DB::table('bps1674_perusahaan')
            ->leftJoin('bps1674_perusahaan_survey', 'bps1674_perusahaan_survey.perusahaan_id', '=', 'bps1674_perusahaan.id')
            ->where($arr_where)
            ->select(DB::RAW('DISTINCT bps1674_perusahaan.*'))
            ->orderBy('bps1674_perusahaan.nama')
            // ->groupBy('bps1674_perusahaan.id', 'bps1674_perusahaan.nama')
            ->paginate();

        $datas->withPath('perusahaan');
        $datas->appends($request->all());
        $model= new \App\Bps1674Perusahaan;

        return view('perusahaan.index',compact('datas', 'nama_perusahaan', 'nama_survey', 
            'tahun_mulai', 'notasi_pendapatan', 'pendapatan', 'kategori', 
            'model', 'notasi_tahun'));
    }

    public function data_survey(Request $request){
        $idnya = '';

        if(strlen($request->get('idnya'))>0)
            $idnya = $request->get('idnya');
            
        $datas = \App\Bps1674PerusahaanSurvey::where('perusahaan_id', '=', $idnya)->get();

        return response()->json([
            'datas'=>$datas,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model= new \App\Bps1674Perusahaan;
        return view('perusahaan.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Bps1674PerusahaanRequest $request)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect('perusahaan/create')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $total_utama = $request->get('total_utama');

        $model= new \App\Bps1674Perusahaan;
        $model->nama=$request->get('nama');
        if(strlen($request->get('deskripsi'))==0) $model->deskripsi = '';
        else $model->deskripsi=$request->get('deskripsi');
        $model->alamat=$request->get('alamat');
        $model->kdprop=$request->get('kdprop');
        $model->kdkab=$request->get('kdkab');
        $model->kdkec=$request->get('kdkec');
        $model->kategori=$request->get('kategori');
        $model->desa=$request->get('desa');
        $model->telepon=$request->get('telepon');
        $model->badan_usaha=$request->get('badan_usaha');
        $model->tahun_mulai=$request->get('tahun_mulai');
        $model->longitude=$request->get('longitude');
        $model->lat=$request->get('lat');

        $model->created_by=Auth::id();
        $model->updated_by=Auth::id();
        
        if($model->save()){
            for($i=1;$i<=$total_utama;++$i){
                if(strlen($request->get('u_nama_surveyau'.$i))>0 && strlen($request->get('u_yearau'.$i))>0 && strlen($request->get('u_nama_petugasau'.$i))>0 && strlen($request->get('u_hp_petugasau'.$i))>0){
                    
                    $model_survey = new \App\Bps1674PerusahaanSurvey;
                    
                    $model_survey->perusahaan_id =  $model->id;
                    $model_survey->nama_survey = $request->get('u_nama_surveyau'.$i);
                    $model_survey->month = $request->get('u_monthau'.$i);
                    $model_survey->year = $request->get('u_yearau'.$i);
                    $model_survey->nama_petugas = $request->get('u_nama_petugasau'.$i);
                    $model_survey->hp_petugas = $request->get('u_hp_petugasau'.$i);
                    $model_survey->pendapatan = $request->get('u_pendapatanau'.$i);
                    $model_survey->pengeluaran = $request->get('u_pengeluaranau'.$i);
                    $model_survey->jumlah_pegawai = $request->get('u_jumlah_pegawaiau'.$i);

                    $model_survey->pemberi_jawaban = $request->get('u_pemberi_jawabanau'.$i);
                    $model_survey->kegiatan_utama = $request->get('u_kegiatan_utamaau'.$i);
                    $model_survey->produk_utama = $request->get('u_produk_utamaau'.$i);
                    $model_survey->status_usaha = $request->get('u_status_usahaau'.$i);
                    $model_survey->periode_pencacahan = $request->get('u_periode_pencacahanau'.$i);
                    
                    $model_survey->created_by=Auth::id();
                    $model_survey->updated_by=Auth::id();
                    $model_survey->save();
                }
            }
        }
        
        return redirect('perusahaan')->with('success', 'Information has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $model = \App\Bps1674Perusahaan::find($id);
        return view('perusahaan.detail',compact('model','id'));
    }

    public function store_survey(Request $request)
    {
        $model = \App\Bps1674PerusahaanSurvey::find($request->get("id"));
        
        if($model==null){
            $model = new \App\Bps1674PerusahaanSurvey;
            $model->perusahaan_id= $request->get("perusahaan_id");
            $model->created_by=Auth::id();
            $model->updated_by=Auth::id();
        }

        $model->nama_survey = $request->get('nama_survey');
        $model->month = $request->get('month');
        $model->year = $request->get('year');
        $model->nama_petugas = $request->get('nama_petugas');
        $model->hp_petugas = $request->get('hp_petugas');
        $model->pendapatan=$request->get('pendapatan');
        $model->pengeluaran=$request->get('pengeluaran');
        $model->jumlah_pegawai=$request->get('jumlah_pegawai');
        $model->pemberi_jawaban=$request->get('pemberi_jawaban');
        $model->kegiatan_utama=$request->get('kegiatan_utama');
        $model->produk_utama=$request->get('produk_utama');
        $model->status_usaha=$request->get('status_usaha');
        $model->periode_pencacahan = $request->get('periode_pencacahan');
        $model->save();
        

        return response()->json(['success'=>'Data berhasil ditambah']);
    }

    public function store_kunjungan(Request $request)
    {
        $model = \App\Bps1674PerusahaanKunjungan::find($request->get("id"));
        
        if($model==null){
            $model = new \App\Bps1674PerusahaanKunjungan;
            $model->perusahaan_survey_id= $request->get("perusahaan_survey_id");
            $model->created_by=Auth::id();
            $model->updated_by=Auth::id();
        }

        $model->nama_pengantar = $request->get('nama_pengantar');
        $model->nama_penerima = $request->get('nama_penerima');
        if(strlen($request->get('tanggal_diserahkan'))>0)
            $model->tanggal_diserahkan = date('Y-m-d', strtotime($request->get('tanggal_diserahkan')));
            
        if(strlen($request->get('tanggal_dikembalikan'))>0)
            $model->tanggal_dikembalikan = date('Y-m-d', strtotime($request->get('tanggal_dikembalikan')));
        
        $model->status_dokumen = $request->get('status_dokumen');
        $model->keterangan = $request->get('keterangan');
        $model->save();
        
        return response()->json(['success'=>'Data berhasil ditambah']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = \App\Bps1674Perusahaan::find($id);
        return view('perusahaan.edit',compact('model','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Bps1674PerusahaanRequest $request, $id)
    {
        if (isset($request->validator) && $request->validator->fails()) {
            return redirect('perusahaan/edit',$id)
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $total_utama = $request->get('total_utama');
        $model= \App\Bps1674Perusahaan::find($id);
        
        $model->nama=$request->get('nama');
        
        if(strlen($request->get('deskripsi'))==0) $model->deskripsi = '';
        else $model->deskripsi=$request->get('deskripsi');

        $model->alamat=$request->get('alamat');
        $model->kdprop=$request->get('kdprop');
        $model->kdkab=$request->get('kdkab');
        $model->kdkec=$request->get('kdkec');
        $model->kategori=$request->get('kategori');
        $model->desa=$request->get('desa');
        $model->telepon=$request->get('telepon');
        $model->badan_usaha=$request->get('badan_usaha');
        $model->tahun_mulai=$request->get('tahun_mulai');
        $model->longitude=$request->get('longitude');
        $model->lat=$request->get('lat');

        $model->updated_by=Auth::id();
        if($model->save()){
            for($i=1;$i<=$total_utama;++$i){
                if(strlen($request->get('u_nama_surveyau'.$i))>0 && strlen($request->get('u_yearau'.$i))>0 && strlen($request->get('u_nama_petugasau'.$i))>0 && strlen($request->get('u_hp_petugasau'.$i))>0){
                    
                    $model_survey = new \App\Bps1674PerusahaanSurvey;
                    
                    $model_survey->perusahaan_id =  $model->id;
                    $model_survey->nama_survey = $request->get('u_nama_surveyau'.$i);
                    $model_survey->month = $request->get('u_monthau'.$i);
                    $model_survey->year = $request->get('u_yearau'.$i);
                    $model_survey->nama_petugas = $request->get('u_nama_petugasau'.$i);
                    $model_survey->hp_petugas = $request->get('u_hp_petugasau'.$i);
                    $model_survey->pendapatan = $request->get('u_pendapatanau'.$i);
                    $model_survey->pengeluaran = $request->get('u_pengeluaranau'.$i);
                    $model_survey->jumlah_pegawai = $request->get('u_jumlah_pegawaiau'.$i);
                    
                    $model_survey->pemberi_jawaban = $request->get('u_pemberi_jawabanau'.$i);
                    $model_survey->kegiatan_utama = $request->get('u_kegiatan_utamaau'.$i);
                    $model_survey->produk_utama = $request->get('u_produk_utamaau'.$i);
                    $model_survey->status_usaha = $request->get('u_status_usahaau'.$i);
                    $model_survey->periode_pencacahan = $request->get('u_periode_pencacahanau'.$i);
                    
                    $model_survey->created_by=Auth::id();
                    $model_survey->updated_by=Auth::id();
                    $model_survey->save();
                }
            }

            $datas = \App\Bps1674PerusahaanSurvey::where('perusahaan_id', '=', $model->id)->get();
            foreach($datas as $data){
                if(strlen($request->get('u_nama_survey'.$data->id))>0 && strlen($request->get('u_month'.$data->id))>0 && strlen($request->get('u_year'.$data->id))>0 && strlen($request->get('u_nama_petugas'.$data->id))>0 && strlen($request->get('u_hp_petugas'.$data->id))>0){
                    $model_survey = \App\Bps1674PerusahaanSurvey::find($data->id);
                    
                    $model_survey->nama_survey = $request->get('u_nama_survey'.$data->id);
                    $model_survey->month = $request->get('u_month'.$data->id);
                    $model_survey->year = $request->get('u_year'.$data->id);
                    $model_survey->nama_petugas = $request->get('u_nama_petugas'.$data->id);
                    $model_survey->hp_petugas = $request->get('u_hp_petugas'.$data->id);
                    $model_survey->pendapatan = $request->get('u_pendapatan'.$data->id);
                    $model_survey->pengeluaran = $request->get('u_pengeluaran'.$data->id);
                    $model_survey->jumlah_pegawai = $request->get('u_jumlah_pegawai'.$data->id);
                    
                    $model_survey->pemberi_jawaban = $request->get('u_pemberi_jawaban'.$data->id);
                    $model_survey->kegiatan_utama = $request->get('u_kegiatan_utama'.$data->id);
                    $model_survey->produk_utama = $request->get('u_produk_utama'.$data->id);
                    $model_survey->status_usaha = $request->get('u_status_usaha'.$data->id);
                    $model_survey->periode_pencacahan = $request->get('u_periode_pencacahan'.$data->id);
                    
                    $model_survey->updated_by=Auth::id();

                    $model_survey->save();
                }
            }
        }
        return redirect('perusahaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Bps1674PerusahaanSurvey::where('perusahaan_id',$id)->delete();
        $model = \App\Bps1674Perusahaan::find($id);
        $model->delete();
        return redirect('perusahaan')->with('success','Information has been  deleted');
    }

    public function destroy_survey($id)
    {
        \App\Bps1674PerusahaanKunjungan::where('perusahaan_survey_id',$id)->delete();
        $model = \App\Bps1674PerusahaanSurvey::find($id);
        $model->delete();
        return response()->json(['success'=>'Sukses']);
    }

    public function destroy_kunjungan($id)
    {
        $model = \App\Bps1674PerusahaanKunjungan::find($id);
        $model->delete();
        return response()->json(['success'=>'Sukses']);
    }
}
