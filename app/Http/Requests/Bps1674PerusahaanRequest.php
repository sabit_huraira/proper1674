<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Bps1674PerusahaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'nama' => 'required',
            'alamat' => 'required',
            'kategori' => 'required',
            'badan_usaha' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'alamat' => 'Alamat',
            'kdprop' => 'Kode Prov',
            'kdkab' => 'Kode Kab',
            'kdkec' => 'Kode Kec',
            'desa' => 'Desa',
            'telepon' => 'No Telepon',
            'badan_usaha' => 'Badan Usaha',
            'tahun_mulai' => 'Tahun Mulai',
            'kategori' => 'Kategori',
            'created_by' => 'Dibuat oleh',
            'updated_by' => 'Terkahir diperbaharui oleh',
            'created_at' => 'Dibuat pada',
            'updated_at' => 'Terakhir diperbaharui pada',
            'longitude' => 'Longitude',
            'lat' => 'Latitude',
        ];
    }
}
