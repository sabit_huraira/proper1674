<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Bps1674PengaduanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'judul' => 'required',
            'isi' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'judul' => 'Judul',
            'isi' => 'Isi',
            'created_by' => 'Dibuat oleh',
            'updated_by' => 'Terkahir diperbaharui oleh',
            'created_at' => 'Dibuat pada',
            'updated_at' => 'Terakhir diperbaharui pada',
        ];
    }
}
