<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Bps1674PerusahaanSurveyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'nama_survey' => 'required',
            'year' => 'required',
            'periode_pencacahan' => 'required',
            'nama_petugas' => 'required',
            'hp_petugas' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'perusahaan_id' => 'Perusahaan',
            'nama_survey' => 'Nama Survey',
            'year' => 'Tahun',
            'month' => 'Bulan',
            'nama_petugas' => 'Nama Petugas',
            'hp_petugas' => 'Nomor HP Petugas',
            'pendapatan' => 'Pendapatan',
            'pengeluaran' => 'Pengeluaran',
            'jumlah_pegawai' => 'Jumlah Pegawai',
            'created_by' => 'Dibuat oleh',
            'updated_by' => 'Terkahir diperbaharui oleh',
            'created_at' => 'Dibuat pada',
            'updated_at' => 'Terakhir diperbaharui pada',
        ];
    }
}
