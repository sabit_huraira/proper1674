<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('testa', function () {
    // print_r(Flysystem::connection('webdav')->listContents('remote.php/webdav/'));
    
    $data = Flysystem::connection('webdav')->put('test.txt', 'hai hai');
    print_r("udahan");
    die();
});

Route::group(['middleware' => ['role:superadmin']], function () {    
    Route::resource('uker','UkerController');
    Route::resource('user','UserController');

    //SPATIE
    Route::resource('role','RoleController');
    Route::resource('permission','PermissionController');
    Route::resource('user_role','UserRoleController');
});


Route::group(['middleware' => ['role:superadmin|tatausaha']], function () {   
});


Route::group(['middleware' => 'auth'], function(){
    Route::resource('perusahaan','PerusahaanController')->except(['show']);
    Route::get('perusahaan/{id}/detail','PerusahaanController@detail');
    Route::post('perusahaan/data_survey','PerusahaanController@data_survey');
    Route::post('perusahaan/store_survey','PerusahaanController@store_survey');
    Route::post('perusahaan/store_kunjungan','PerusahaanController@store_kunjungan');
    
    Route::get('perusahaan/{id}/destroy_survey', 'PerusahaanController@destroy_survey');
    Route::get('perusahaan/{id}/destroy_kunjungan', 'PerusahaanController@destroy_kunjungan');

    Route::resource('pengaduan','PengaduanController')->except(['show']);
    Route::get('pengaduan/{id}/detail','PengaduanController@detail');
    Route::get('pengaduan/{id}/create_respon','PengaduanController@create_respon');
    Route::post('pengaduan/{id}/store_respon','PengaduanController@store_respon');
    
    Route::get('data/index','DataController@index');
    Route::get('data/publikasi','DataController@publikasi');
    Route::get('data/berita/','DataController@berita');
    Route::get('data/dinamis/','DataController@dinamis');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('guest', 'HomeController@guest')->name('guest');
