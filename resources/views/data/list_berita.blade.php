
<div id="berita">
    <div class="table-responsive">
        <table class="table-bordered m-b-0" style="min-width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Berita</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="(data, index) in berita" :key="data.news_id">
                    <td>@{{ index+1 }}</td>
                    <td>
                        @{{ data.title }}<br/>
                        <small class="text-muted">(Tanggal Rilis: @{{ data.rl_date }})</small>
                    </td>
                    <td class="text-center">
                        <div v-html="data.news"></div>
                        <a :href="urlBerita(data.title, data.rl_date, data.news_id)">selengkapnya</a>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <br/>
        <ul id="pagination-demo" class="pagination-sm"></ul>
    </div>
</div>