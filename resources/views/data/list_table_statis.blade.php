
<div id="table_statis">
    <div class="table-responsive">
        <table class="table-bordered m-b-0" style="min-width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Subject</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Unduh</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="(data, index) in table_statis" :key="data.table_id">
                    <td>@{{ index+1 }}</td>
                    <td>@{{ data.subject }}</td>
                    <td>@{{ data.title }}</td>
                    <td class="text-center">
                        <a :href="data.excel">Unduh</a> <small class="text-muted">(size: @{{ data.size }})</small>
                        <img :src="'http://www.barcodes4.me/barcode/qr/bps.png?value='+data.excel+'&ecclevel=0'"></img> 
                    </td>
                </tr>
            </tbody>
        </table>
        
        <br/>

        <ul id="pagination-demo" class="pagination-sm"></ul>
    </div>
</div>