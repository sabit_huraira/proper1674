
<div class="tab-pane" id="subject">
    <div class="table-responsive">
        <table class="table-bordered m-b-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Subkategori</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="(data, index) in subject" :key="data.sub_id">
                    <td>@{{ index+1 }}</td>
                    <td>@{{ data.title }}</td>
                    <td>@{{ data.subcat }}</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>