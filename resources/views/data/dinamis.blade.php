@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="icon-home"></i></a></li>                     
    <li class="breadcrumb-item">Data BPS -  Tabel Dinamis</li>
</ul>
@endsection

@section('content')
    <div class="container"  id="app_vue">
      <br />
      @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
        </div><br />
      @endif

      <div class="card">
        <div class="body">
            <table class="m-b-0" style="min-width:100%">
                <tbody>
                    <tr>
                        <td>Pilih Kategori</td>
                        <td>                       
                            <select class="form-control" v-model="kategori" @change="setSubject">
                                <option value="">- Pilih Kategori -</option>
                                <option v-for="(data, index) in list_kategori" :key="data.subcat_id" 
                                    :value="data.subcat_id">
                                    @{{ data.title }}
                                </option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>Pilih Subject</td>
                        <td>                       
                            <select class="form-control" v-model="subject" @change="setVar">
                                <option value="">- Pilih Subject -</option>
                                <option v-for="(data, index) in list_subject" :key="data.sub_id" 
                                    :value="data.sub_id">
                                    @{{ data.title }}
                                </option>
                            </select>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>Pilih Variabel</td>
                        <td>                       
                            <select class="form-control" v-model="varvar">
                                <option value="">- Pilih Variabel -</option>
                                <option v-for="(data, index) in list_varvar" :key="data.var_id" 
                                    :value="data.var_id">
                                    @{{ data.title }}
                                </option>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br/><br/>
          
      </div>
    </div>


      <div class="modal hide" id="wait_progres" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-body">
                      <div class="text-center"><img src="{!! asset('lucid/assets/images/loading.gif') !!}" width="200" height="200" alt="Loading..."></div>
                      <h4 class="text-center">Please wait...</h4>
                  </div>
              </div>
          </div>
      </div>

  </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{!! asset('lucid/assets/vendor/pagination/twbs-pagination-master/jquery.twbsPagination.min.js') !!}"></script>
    <script>
        var vm = new Vue({
            el: "#app_vue",
            data:  {
                list_kategori: [], kategori: '',
                list_subject: [], subject: '',
                list_varvar: [], varvar: '',
                api_path: 'https://webapi.bps.go.id/v1/api/list',
                api_key: '9a1097f6a7994990a7a8db0e463615ce',
                domain: '1674',
            },
            methods: {
                setKategori: function(){
                    var self = this;
                    $('#wait_progres').modal('show');
                    
                    $.ajax({
                        url : self.api_path, method : 'get', dataType: 'json',
                        data:{
                            model: 'subcat', lang: 'ind', domain: self.domain, key: self.api_key
                        },
                    }).done(function (data) {
                        self.list_kategori = data.data[1]; 
                        // self.kategori = self.list_kategori[1].subcat_id;
                        
                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg)); $('#wait_progres').modal('hide');
                    });
                },
                setSubject: function(){
                    var self = this;
                    $('#wait_progres').modal('show');
                    
                    if(self.kategori==''){
                        self.list_subject = [];
                    }
                    else{
                        $.ajax({
                            url : self.api_path, method : 'get', dataType: 'json',
                            data:{
                                model: 'subject', lang: 'ind', domain: self.domain, 
                                key: self.api_key, perpage: 40, subcat: self.kategori,
                            },
                        }).done(function (data) {
                            self.list_subject = data.data[1]; 
                            // self.subject = self.list_subject[1].cat_id;

                            self.list_varvar = [];
                            self.varvar = '';
                            
                            $('#wait_progres').modal('hide');
                        }).fail(function (msg) {
                            console.log(JSON.stringify(msg)); $('#wait_progres').modal('hide');
                        });
                    }
                },
                setVar: function(){
                    var self = this;
                    $('#wait_progres').modal('show');
                    
                    $.ajax({
                        url : self.api_path, method : 'get', dataType: 'json',
                        data:{
                            model: 'var', lang: 'ind', domain: self.domain,
                            key: self.api_key, subject: self.subject,
                        },
                    }).done(function (data) {
                        self.list_varvar = data.data[1]; 
                        // self.varvar = self.list_varvar[1].var_id;
                        
                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg)); $('#wait_progres').modal('hide');
                    });
                },
                
            }
        });

        $(document).ready(function() {
            vm.setKategori();
        });
    </script>
@endsection
