
<div class="tab-pane" id="brs">
    <div class="table-responsive">
        <table class="table-bordered m-b-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Tanggal Rilis</th>
                    <th class="text-center">Unduh</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="(data, index) in brs" :key="data.brs_id">
                    <td>@{{ index+1 }}</td>
                    <td>@{{ data.title }}</td>
                    <td class="text-center">@{{ data.rl_date }}</td>
                    <td class="text-center"><a :href="data.pdf">Unduh</a> <small class="text-muted">(size: @{{ data.size }})</small></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>