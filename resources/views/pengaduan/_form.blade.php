<div id="app_vue">
    <div class="form-group">
        <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['judul']}}</label>
        <input type="text" class="form-control {{($errors->first('judul') ? ' parsley-error' : '')}}" required name="judul" value="{{ old('judul', $model->judul) }}">
        @foreach ($errors->get('judul') as $msg)
            <p class="text-danger">{{ $msg }}</p>
        @endforeach
    </div>

    <div class="form-group">
        <label>{{ $model->attributes()['isi']}}</label>
        <textarea class="form-control {{($errors->first('isi') ? ' parsley-error' : '')}}" name="isi" rows="6">{{ old('isi', $model->isi) }}</textarea>
        @foreach ($errors->get('isi') as $msg)
            <p class="text-danger">{{ $msg }}</p>
        @endforeach
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Simpan</button>
</div>

@section('css')
    <meta name="_token" content="{{csrf_token()}}" />
    <meta name="csrf-token" content="@csrf">
    <link rel="stylesheet" href="{!! asset('lucid/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}">
@endsection

@section('scripts')
    <script src="{!! asset('lucid/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
@endsection