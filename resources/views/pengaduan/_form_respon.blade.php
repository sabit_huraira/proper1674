<div id="app_vue">
    <div class="form-group">
        <label>Balasan:</label>
        <textarea class="form-control {{($errors->first('isi') ? ' parsley-error' : '')}}" name="isi" rows="6">{{ old('isi', $model->isi) }}</textarea>
        @foreach ($errors->get('isi') as $msg)
            <p class="text-danger">{{ $msg }}</p>
        @endforeach
    </div>

    <br>
    <button type="submit" class="btn btn-primary">Simpan</button>
</div>