@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{url('pengaduan')}}">Pengaduan</a></li>                            
    <li class="breadcrumb-item">Tambah Balasan</li>
</ul>
@endsection

@section('content')
<div class="row clearfix">
  <div class="col-md-12">
      <div class="card">
          <div class="header">
              <h2>Tambah Pengaduan</h2>
          </div>
          <div class="body">
                <div date-is="20-04-2018 - Today">
                    <h5>{{ $parent->judul }}</h5>
                    <span><a href="#">{{ $parent->createdUser->name }}</a> <small class="float-right">{{ $parent->created_at->diffForHumans() }}</small></span>
                    <div class="msg">
                        <p>{{ $parent->isi }}</p>
                    </div>                                
                </div>
                <hr/>
              <form method="post" action="{{url('pengaduan/'.$parent->id.'/store_respon')}}" enctype="multipart/form-data">
              @csrf
              @include('pengaduan._form_respon')
              </form>
          </div>
      </div>
  </div>
</div>
@endsection
