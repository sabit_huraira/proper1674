@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="icon-home"></i></a></li>                     
    <li class="breadcrumb-item">Pengaduan</li>
</ul>
@endsection

@section('content')
    <div class="container">
      <br />
      @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
        </div><br />
      @endif

      <div class="card">
        <div class="body">
          <a href="{{action('PengaduanController@create')}}" class="btn btn-info">Tambah Pengaduan</a>
          <br/><hr/>
          <form action="{{url('pengaduan')}}" method="get">
            <div class="input-group mb-3">     
              @csrf
              <input type="text" class="form-control" name="search" id="search" value="{{ $keyword }}" placeholder="Search..">

              <div class="input-group-append">
                  <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
              </div>
              </div>
          </form>
      </div>
    </div>

    <div class="card">
        <div class="body">
          <section class="datas">
            @include('pengaduan.list')
          </section>
          
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/pagination.js') !!}"></script>
@endsection
