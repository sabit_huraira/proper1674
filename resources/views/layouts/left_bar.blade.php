
    <div id="left-sidebar" class="sidebar">
        <div class="sidebar-scroll">
            <div class="user-account">
                <img src="{!! Auth::user()->fotoUrl !!}" class="rounded-circle user-photo" alt="User Profile Picture">
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong>{{ Auth::user()->name }}</strong></a>
                    <ul class="dropdown-menu dropdown-menu-right account">
                        <!-- <li><a href="#"><i class="icon-user"></i>My Profile</a></li>
                        <li><a href="#"><i class="icon-envelope-open"></i>Messages</a></li>
                        <li><a href="#"><i class="icon-settings"></i>Settings</a></li>
                        <li class="divider"></li> -->
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
                <hr>
                <ul class="row list-unstyled">
                    <li class="col-12">
                        <h6>#DataPerusahaan</h6>
                    </li>
                </ul>
            </div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu">Menu</a></li>
             </ul>
                
            <!-- Tab panes -->
            <div class="tab-content p-l-0 p-r-0">
                <div class="tab-pane active" id="menu">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">   
                            
                            <li class="{{ (request()->is('perusahaan*')) ? 'active' : '' }}">
                                <a href="{{ url('perusahaan') }}" > <i class="icon-layers"></i><span>Data Perusahaan</span></a>
                            </li>

                            <li class="{{ (request()->is('data*')) ? 'active' : '' }}">
                                <a href="#data" class="has-arrow"><i class="icon-basket-loaded"></i> <span>Data BPS</span></a>
                                <ul>                                  
                                    <li class="{{ request()->is('data/index*') ? 'active' : '' }}"><a href="{{ url('data/index') }}">Tabel Statis</a></li>
                                    <li class="{{ request()->is('data/dinamis*') ? 'active' : '' }}"><a href="{{ url('data/dinamis') }}">Tabel Dinamis</a></li>
                                    <li class="{{ request()->is('data/publikasi*') ? 'active' : '' }}"><a href="{{ url('data/publikasi') }}">Publikasi</a></li>
                                    <li class="{{ request()->is('data/berita*') ? 'active' : '' }}"><a href="{{ url('data/berita') }}">Berita</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>          
            </div>          
        </div>
    </div>