<b>Daftar Survey</b> -
&nbsp <a href="#" id="add-utama" data-toggle="modal" data-target="#form_survey">Tambah Survey &nbsp &nbsp<i class="icon-plus text-info"></i></a>
<br/>
<small class="text-muted font-italic font-weight-lighter float-left">*Isian kunjungan survey dapat diisi pada "detail" data perusahaan.</small>

<div class="table-responsive">
    <table class="table m-b-0 table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th class="text-center">Nama Survey</th>
                <th class="text-center">Daftar Kunjungan</th>    
                <th class="text-center">Keterangan</th>   
            </tr>
        </thead>

        <tbody>
            <tr v-for="(data, index) in datas" :key="data.id">
                <td>
                    <template v-if="is_delete(data.id)">
                        <a :data-id="data.id" v-on:click="delData(data.id)"><i class="fa fa-trash text-danger"></i>&nbsp </a>
                    </template>
                    
                    <template v-if="!is_delete(data.id)">
                        <a :data-id="data.id" v-on:click="delDataTemp(index)"><i class="fa fa-trash text-danger"></i>&nbsp </a>
                    </template>

                    <a href="#" role="button" v-on:click="updateSurvey" data-toggle="modal" 
                            :data-id="data.id" :data-nama_survey="data.nama_survey" 
                            :data-month="data.month" :data-year="data.year" 
                            :data-nama_petugas="data.nama_petugas" :data-hp_petugas="data.hp_petugas" 
                            :data-pendapatan="data.pendapatan" :data-pengeluaran="data.pengeluaran" 
                            :data-jumlah_pegawai="data.jumlah_pegawai" :data-pemberi_jawaban="data.pemberi_jawaban" 
                            :data-kegiatan_utama="data.kegiatan_utama" :data-produk_utama="data.produk_utama" 
                            :data-status_usaha="data.status_usaha" :data-periode_pencacahan="data.periode_pencacahan" 
                            data-target="#form_survey"> <i class="icon-pencil"></i></a>
                    @{{ index+1 }}
                </td>

                <td>@{{ data.nama_survey }} (pemberi jawaban: @{{ data.pemberi_jawaban }} )<br/>
                    Periode Pencacahan: @{{ list_survey[data.periode_pencacahan] }} <br/>
                    
                    
                    <div v-if="data.periode_pencacahan != 4 && data.periode_pencacahan != 0"> 
                        <div v-if="data.periode_pencacahan == 1">Bulan: @{{ data.month }} <br/></div>
                        <div v-else-if="data.periode_pencacahan == 2">Triwulan: @{{ data.month }} <br/></div>
                        <div v-else-if="data.periode_pencacahan == 3">Semester: @{{ data.month }} <br/></div>   
                    </div>
                    
                    Tahun: @{{ data.year }} <br/>
                    Petugas & No hp: @{{ data.nama_petugas }} (@{{ data.hp_petugas }})<br/>
                
                <td>
                    <a href="#" id="add-survey" data-toggle="modal" :data-survey="data.id" v-on:click="buatKunjungan" data-target="#form_kunjungan">Tambah Kunjungan &nbsp &nbsp<i class="icon-plus text-info"></i></a>
                    <div v-for="(data2, index2) in data.kunjungan" :key="data2.id">
                        <hr/>
                        <p>
                            <a href="#" role="button" v-on:click="updateKunjungan" data-toggle="modal" 
                                    :data-id="data2.id" :data-nama_pengantar="data2.nama_pengantar" 
                                    :data-nama_penerima="data2.nama_penerima" 
                                    :data-tanggal_diserahkan="data2.tanggal_diserahkan" 
                                    :data-tanggal_dikembalikan="data2.tanggal_dikembalikan" 
                                    :data-status_dokumen="data2.status_dokumen" 
                                    :data-keterangan="data2.keterangan"
                                    data-target="#form_kunjungan"> <i class="icon-pencil"></i></a>

                            (@{{ data2.tanggal_diserahkan }}) Pengirim:  @{{ data2.nama_pengantar }}, penerima: @{{ data2.nama_penerima }}</p>
                        <p>Status dokumen: @{{ data2.status_dokumen }}, dikembalikan pada:  @{{ data2.tanggal_dikembalikan }} (@{{ data2.keterangan }})</p>
                    </div>
                </td>
                <td>
                    Kegiatan Utama: @{{ data.kegiatan_utama }} <br/>
                    Produk Utama: @{{ data.produk_utama }} <br/>
                    Status Usaha: @{{ (data.status_usaha==1 ? "Aktif" : "Tidak Aktif") }} <br/>
                    Pendapatan: Rp. @{{ moneyFormat(data.pendapatan) }} <br/>
                    Pengeluaran: Rp. @{{ moneyFormat(data.pengeluaran) }} <br/>
                    Jumlah Pegawai: @{{ data.jumlah_pegawai }} 
                </td>  
            </tr>
        </tbody>
    </table>
</div>