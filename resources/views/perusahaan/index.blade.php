@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="icon-home"></i></a></li>                     
    <li class="breadcrumb-item">Perusahaan</li>
</ul>
@endsection

@section('content')
    <div class="container">
      <br />
      @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
        </div><br />
      @endif

      <div class="card">
        <div class="body">
          <a href="{{action('PerusahaanController@create')}}" class="btn btn-info">Tambah</a>
          <br/><hr/>
          <form action="{{url('perusahaan')}}" method="get">
                    
              @csrf
                <div class="row clearfix">
                    <div class="col-md-4">
                      <label for="filter">Nama Perusahaan:</label>
                      <div class="form-group">                                                        
                          <input class="form-control" type="text" name="nama_perusahaan" value="{{ $nama_perusahaan }}" />
                      </div>
                    </div>
                
                    <div class="col-md-4">
                      <label for="filter">Survey yang pernah diikuti:</label>
                      <div class="form-group">                                                        
                          <input class="form-control" type="text" name="nama_survey" value="{{ $nama_survey }}" />
                      </div>
                    </div>
                    
                    <div class="col-md-4">
                      <label for="filter">Kategori:</label>
                      <div class="form-group">                                                        
                        <select class="form-control" name="kategori">
                            <option value="">- Pilih Kategori -</option>
                            @foreach ($model->ListKategori as $key=>$value)
                                <option value="{{ $key }}" 
                                    @if ($key == $kategori)
                                        selected="selected"
                                    @endif >{{ $key }} - {{ $value }}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                </div>

                
                <div class="row clearfix">
                    <div class="col-md-6">
                      <label for="filter">Tahun mulai:</label>
                      
                      <div class="row clearfix">
                          <div class="col-md-3">
                            <select class="form-control" name="notasi_tahun">
                                <option value="0" @if ($notasi_tahun == 0) selected="selected" @endif >>=</option>
                                <option value="1" @if ($notasi_tahun == 1) selected="selected" @endif>=</option>
                                <option value="2" @if ($notasi_tahun == 2) selected="selected" @endif><=</option>
                            </select>
                          </div>
                          
                          <div class="col-md-9">
                            <input class="form-control" type="number" name="tahun_mulai" value="{{ $tahun_mulai }}" />
                          </div>                      
                      </div>
                    </div>
                
                    <div class="col-md-6">
                      <label for="filter">Omzet/Pendapatan:</label>
                      
                        <div class="row clearfix">     
                          
                          <div class="col-md-3">
                            <select class="form-control" name="notasi_pendapatan">
                                <option value="0" @if ($notasi_pendapatan == 0) selected="selected" @endif >>=</option>
                                <option value="1" @if ($notasi_pendapatan == 1) selected="selected" @endif>=</option>
                                <option value="2" @if ($notasi_pendapatan == 2) selected="selected" @endif><=</option>
                            </select>
                          </div>
                          
                          <div class="col-md-9">
                            <input class="form-control" type="number" name="pendapatan" value="{{ $pendapatan }}" />
                          </div>                      
                      </div>
                    </div>
                </div>
                <br/>
              <button type="submit" class="btn btn-primary btn-block">Search</button>
            
          </form>
          
      </div>
    </div>

    
    <div class="card">
        <div class="body">
          <section class="datas">
            @include('perusahaan.list')
          </section>
          
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript" src="{!! asset('js/pagination.js') !!}"></script>
@endsection
