<div class="modal fade" id="form_kunjungan" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>Form Kunjungan</b>
            </div>
            <div class="modal-body">
                <input type="hidden" v-model="fk_id">

                <div class="row clearfix">
                    <div class="col-md-12">
                        Nama Pengantar:
                        <div class="form-line">
                            <input type="text" v-model="fk_nama_pengantar" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        Nama Penerima:
                        <div class="form-line">
                            <input type="text" v-model="fk_nama_penerima" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Tanggal diserahkan:
                        <div class="form-line">
                            <div class="input-group">
                                <input type="text" class="datepicker form-control" id="tanggal_diserahkan">
                                <div class="input-group-append">                                            
                                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        Tanggal dikembalikan:
                        <div class="form-line">
                            <div class="input-group">
                                <input type="text" class="datepicker form-control" id="tanggal_dikembalikan">
                                <div class="input-group-append">                                            
                                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-calendar"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Status Dokumen:
                        <div class="form-line">
                            <select class="form-control" v-model="fk_status_dokumen" >
                                <option value="1">Belum Kembali</option>
                                <option value="2">Telah dikembalikan</option>
                            </select>
                        </div>
                    </div>
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-6">
                        Keterangan:
                        <div class="form-line">
                            <input type="text" v-model="fk_keterangan" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6"></div>
                </div>
                          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" v-on:click="saveKunjungan">SAVE</button>
                <button type="button" class="btn btn-simple" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>