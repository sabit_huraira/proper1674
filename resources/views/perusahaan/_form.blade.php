<div id="app_vue">
    <div class="row clearfix">
        <div class="col-md-6">
            <div class="form-group">
                <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['nama']}}</label>
                <input type="text" class="form-control {{($errors->first('nama') ? ' parsley-error' : '')}}" required name="nama" value="{{ old('nama', $model->nama) }}">
                @foreach ($errors->get('nama') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['kategori']}}</label>
                <select class="form-control" name="kategori" required>
                    @foreach ($model->ListKategori as $key=>$value)
                        <option value="{{ $key }}" 
                            @if ($key == old('kategori', $model->kategori))
                                selected="selected"
                            @endif >{{ $key }} - {{ $value }}</option>
                    @endforeach
                </select>
                @foreach ($errors->get('kategori') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['deskripsi']}}</label>
                <textarea class="form-control {{($errors->first('deskripsi') ? ' parsley-error' : '')}}" name="deskripsi" rows="6">{{ old('deskripsi', $model->deskripsi) }}</textarea>
                @foreach ($errors->get('deskripsi') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="form-group">
                <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['alamat']}}</label>
                <textarea class="form-control {{($errors->first('alamat') ? ' parsley-error' : '')}}"  required name="alamat" rows="6">{{ old('alamat', $model->alamat) }}</textarea>
                @foreach ($errors->get('alamat') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-4">
            <div class="form-group">
                <label>{{ $model->attributes()['kdprop']}}</label>
                <input type="text" class="form-control {{($errors->first('kdprop') ? ' parsley-error' : '')}}" name="kdprop" value="{{ old('kdprop', $model->kdprop) }}">
                @foreach ($errors->get('kdprop') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>{{ $model->attributes()['kdkab']}}</label>
                <input type="text" class="form-control {{($errors->first('kdkab') ? ' parsley-error' : '')}}" name="kdkab" value="{{ old('kdkab', $model->kdkab) }}">
                @foreach ($errors->get('kdkab') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>{{ $model->attributes()['kdkec']}}</label>
                <input type="text" class="form-control {{($errors->first('kdkec') ? ' parsley-error' : '')}}" name="kdkec" value="{{ old('kdkec', $model->kdkec) }}">
                @foreach ($errors->get('kdkec') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['desa']}}</label>
                <input type="text" class="form-control {{($errors->first('desa') ? ' parsley-error' : '')}}" name="desa" value="{{ old('desa', $model->desa) }}">
                @foreach ($errors->get('desa') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['telepon']}}</label>
                <input type="text" class="form-control {{($errors->first('telepon') ? ' parsley-error' : '')}}" name="telepon" value="{{ old('telepon', $model->telepon) }}">
                @foreach ($errors->get('telepon') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-6">
            <div class="form-group">
                <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['badan_usaha']}}</label>
                <select class="form-control" name="badan_usaha">
                    @foreach ($model->ListBadanUsaha as $key=>$value)
                        <option value="{{ $key }}" 
                            @if ($key == old('badan_usaha', $model->badan_usaha))
                                selected="selected"
                            @endif >{{ $value }}</option>
                    @endforeach
                </select>
                @foreach ($errors->get('badan_usaha') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['tahun_mulai']}}</label>
                <input type="number" class="form-control {{($errors->first('tahun_mulai') ? ' parsley-error' : '')}}" name="tahun_mulai" value="{{ old('tahun_mulai', $model->tahun_mulai) }}">
                @foreach ($errors->get('tahun_mulai') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['longitude']}}</label>
                <input type="text" class="form-control {{($errors->first('longitude') ? ' parsley-error' : '')}}" name="longitude" value="{{ old('longitude', $model->longitude) }}">
                @foreach ($errors->get('longitude') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>{{ $model->attributes()['lat']}}</label>
                <input type="text" class="form-control {{($errors->first('lat') ? ' parsley-error' : '')}}" name="lat" value="{{ old('lat', $model->lat) }}">
                @foreach ($errors->get('lat') as $msg)
                    <p class="text-danger">{{ $msg }}</p>
                @endforeach
            </div>
        </div>
    </div>
    

    @include('perusahaan.list_survey')
    @include('perusahaan.modal_form_survey')
    <input type="hidden" name="total_utama" v-model="total_utama">

    <br>
    <button type="submit" class="btn btn-primary">Simpan</button>
</div>

@section('css')
    <meta name="_token" content="{{csrf_token()}}" />
    <meta name="csrf-token" content="@csrf">
    <link rel="stylesheet" href="{!! asset('lucid/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}">
@endsection

@section('scripts')
    <script src="{!! asset('lucid/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>

    <script>
        var vm = new Vue({
            el: "#app_vue",
            data:  {
                total_utama: 1,
                datas: [],
                idnya: {!! json_encode($model->id) !!},
                f_id: '', f_nama_survey: '', f_month: '', f_year: '', f_nama_petugas: '',
                f_hp_petugas: '',f_pendapatan: '' ,f_pengeluaran: '', f_jumlah_pegawai: '',
                f_pemberi_jawaban: '',f_kegiatan_utama: '' ,f_produk_utama: '', f_status_usaha: '',
                f_periode_pencacahan: 1,
                f_index: -1,

                
                list_survey: {!! json_encode($model->ListPeriodeSurvey) !!},
                list_kategori: {!! json_encode($model->ListKategori) !!},
            },
            computed: {
                pathname: function () {
                    if(this.idnya)
                        return (window.location.pathname).replace("/"+this.idnya+"/edit", "");
                    else
                        return (window.location.pathname).replace("/create", "");
                },
            },
            methods: {
                moneyFormat:function(amount){
                    var decimalCount = 0;
                    var decimal = ".";
                    var thousands = ",";
                    decimalCount = Math.abs(decimalCount);
                    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                    const negativeSign = amount < 0 ? "-" : "";

                    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                    let j = (i.length > 3) ? i.length % 3 : 0;

                    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");      
                },
                setDatas: function(){
                    var self = this;
                    console.log(self.pathname+"/data_survey");
                    $('#wait_progres').modal('show');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    
                    $.ajax({
                        url : self.pathname+"/data_survey",
                        method : 'post',
                        dataType: 'json',
                        data:{
                            idnya: self.idnya,
                        },
                    }).done(function (data) {
                        self.datas = data.datas;
                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg));
                        $('#wait_progres').modal('hide');
                    });
                },
                is_delete: function(params){
                    if(isNaN(params)) return false;
                    else return true;
                },
                updateSurvey: function (event) {
                    var self = this;

                    if (event) {
                        self.f_id = event.currentTarget.getAttribute('data-id');
                        self.f_nama_survey = event.currentTarget.getAttribute('data-nama_survey');
                        self.f_month = event.currentTarget.getAttribute('data-month');
                        self.f_year = event.currentTarget.getAttribute('data-year');
                        self.f_nama_petugas = event.currentTarget.getAttribute('data-nama_petugas');
                        self.f_hp_petugas = event.currentTarget.getAttribute('data-hp_petugas');
                        self.f_pendapatan = event.currentTarget.getAttribute('data-pendapatan');
                        self.f_pengeluaran = event.currentTarget.getAttribute('data-pengeluaran');
                        self.f_jumlah_pegawai = event.currentTarget.getAttribute('data-jumlah_pegawai');
                        self.f_pemberi_jawaban = event.currentTarget.getAttribute('data-pemberi_jawaban');
                        self.f_kegiatan_utama = event.currentTarget.getAttribute('data-kegiatan_utama');
                        self.f_produk_utama = event.currentTarget.getAttribute('data-produk_utama');
                        self.f_status_usaha = event.currentTarget.getAttribute('data-status_usaha');
                        self.f_periode_pencacahan = event.currentTarget.getAttribute('data-periode_pencacahan');
                        self.f_index = event.currentTarget.getAttribute('data-index');
                    }
                },
                saveSurvey: function(){
                    var self = this;
                    
                    if(self.f_nama_survey.length==0 || self.f_year.length==0 || 
                        self.f_nama_petugas.length==0 || self.f_hp_petugas.length==0){
                        alert("Pastikan nama survey, tahun, bulan, nama petugas dan hp petugas telah diisi");
                    }
                    else{
                        if(isNaN(self.f_jumlah_pegawai) || isNaN(self.f_pendapatan) || isNaN(self.f_pengeluaran) || 
                            isNaN(self.f_month) || isNaN(self.f_year)){
                            alert("Isian jumlah pegawai, bulan, tahun, pendapatan atau pengeluaran harus angka");
                        }
                        else{
                            if(self.f_id==''){
                                self.datas.push({
                                    'id': 'au'+(self.total_utama),
                                    'nama_survey'   : self.f_nama_survey,
                                    'month'   : self.f_month,
                                    'year'   : self.f_year,
                                    'nama_petugas'   : self.f_nama_petugas,
                                    'hp_petugas'   : self.f_hp_petugas,
                                    'pendapatan'   : self.f_pendapatan,
                                    'pengeluaran'     : self.f_pengeluaran,
                                    'jumlah_pegawai'     : self.f_jumlah_pegawai,
                                    'pemberi_jawaban'     : self.f_pemberi_jawaban,
                                    'kegiatan_utama'     : self.f_kegiatan_utama,
                                    'produk_utama'     : self.f_produk_utama,
                                    'status_usaha'     : self.f_status_usaha,
                                    'periode_pencacahan'     : self.f_periode_pencacahan,
                                });

                                self.total_utama++;
                            }
                            else{
                                self.datas[self.f_index] = {
                                    'id': self.f_id,
                                    'nama_survey'   : self.f_nama_survey,
                                    'month'   : self.f_month,
                                    'year'   : self.f_year,
                                    'nama_petugas'   : self.f_nama_petugas,
                                    'hp_petugas'   : self.f_hp_petugas,
                                    'pendapatan'   : self.f_pendapatan,
                                    'pengeluaran'     : self.f_pengeluaran,
                                    'jumlah_pegawai'     : self.f_jumlah_pegawai,
                                    'pemberi_jawaban'     : self.f_pemberi_jawaban,
                                    'kegiatan_utama'     : self.f_kegiatan_utama,
                                    'produk_utama'     : self.f_produk_utama,
                                    'status_usaha'     : self.f_status_usaha,
                                    'periode_pencacahan'     : self.f_periode_pencacahan,
                                };
                            }
                            
                            self.f_id = '';
                            self.f_nama_survey = '';
                            self.f_month = '';
                            self.f_year = '';
                            self.f_nama_petugas = '';
                            self.f_hp_petugas = '';
                            self.f_pendapatan = '';
                            self.f_pengeluaran = '';
                            self.f_jumlah_pegawai = '';
                            self.f_pemberi_jawaban = '';
                            self.f_kegiatan_utama = '';
                            self.f_produk_utama = '';
                            self.f_status_usaha = '';
                            self.f_periode_pencacahan = 1;
                            
                            $('#form_survey').modal('hide');
                        }  
                    }
                },
                delData: function (idnya) {
                    var self = this;
                    if(confirm("Anda yakin ingin menghapus data ini?")){
                        $('#wait_progres').modal('show');
                        $.ajax({
                            url : self.pathname+"/" + idnya + "/destroy_survey",
                            method : 'get',
                            dataType: 'json',
                        }).done(function (data) {
                            window.location.reload(true);
                            $('#wait_progres').modal('hide');
                        }).fail(function (msg) {
                            console.log(JSON.stringify(msg));
                            $('#wait_progres').modal('hide');
                        });
                    }
                },
                delDataTemp: function (index) {
                    var self = this;
                    
                    if(confirm("Anda yakin ingin menghapus data ini?")){
                        $('#wait_progres').modal('show');
                        self.rincian.splice(index, 1);
                        $('#wait_progres').modal('hide');
                    }
                },
            }
        });

        $(document).ready(function() {
            $('.datepicker').datepicker({});
            vm.setDatas();
        });
    </script>
@endsection