<div id="load" class="table-responsive">
    <table class="table m-b-0">
        @if (count($datas)==0)
            <thead>
                <tr><th>Tidak ditemukan data</th></tr>
            </thead>
        @else
            <thead>
                <tr>
                <th>Nama Perusahaan</th>
                <th>Alamat</th>
                <th>Kategori</th>
                <th class="text-center" colspan="3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>{{ $data->nama }}</td>
                    <td>{!! $data->alamat !!}</td>
                    <td>{!! $data->kategori !!}</td>
                    
                    <td class="text-center"><a href="{{action('PerusahaanController@detail', $data->id)}}"><i class="icon-magnifier text-info"></i></a></td>
                    <td class="text-center"><a href="{{action('PerusahaanController@edit', $data->id)}}"><i class="icon-pencil text-info"></i></a></td>
                    <td class="text-center">
                        <form  class="delete" action="{{action('PerusahaanController@destroy', $data->id)}}" method="post">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button type="submit"><i class="icon-trash text-danger"></i></button>
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        @endif
    </table>
    {{ $datas->links() }} 
</div>


@section('scripts')
<script>
 $(".delete").on("submit", function(){
    return confirm("Anda yakin ingin menghapus data ini?");
});
</script>
@endsection